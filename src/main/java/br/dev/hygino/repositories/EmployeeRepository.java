package br.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.dev.hygino.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
