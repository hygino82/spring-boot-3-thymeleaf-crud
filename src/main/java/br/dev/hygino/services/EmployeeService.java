package br.dev.hygino.services;

import java.util.List;

import org.springframework.data.domain.Page;

import br.dev.hygino.models.Employee;

public interface EmployeeService {
    List<Employee> getAllEmployees();

    void saveEmployee(Employee employee);

    Employee getEmployeeById(long id);

    void deleteEmployeeById(long id);

    Page<Employee> findPaginated(int pageNumber, int pageSize, String sortField, String sortDirection);
}
